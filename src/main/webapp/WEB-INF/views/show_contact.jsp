<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<title>SpringBootRocks</title>
<%@ include file="../fragments/header.jspf"%>
<%@ include file="../fragments/navbar.jspf"%>
</head>
<body>
	<!-- 	<div class="container-fluid"> -->
	<div class="container">
		<div style="text-align: center">
			<h4>
				<b>Contacts</b>
			</h4>
		</div>
		<table id="tableitems"
			class="table table-condensed table_morecondensed table-striped table-responsive-sm  width=80%">
			<thead class="thead-secondary">
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Phone</th>
				</tr>
			</thead>
			<c:forEach items="${contactForm.contacts}" var="contact"
				varStatus="status">
				<tr>
					<td>${contact.firstname}</td>
					<td>${contact.lastname}</td>
					<td>${contact.email}</td>
					<td>${contact.phone}</td>
				</tr>
			</c:forEach>
		</table>
		<br /> <input type="button" class="btn btn-danger" value="Back"
			onclick="javascript:history.back()" />


	</div>
</body>

<%@ include file="../fragments/footer.jspf"%>
</html>