<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<title>SpringBootRocks</title>
<%@ include file="../fragments/header.jspf"%>
<%@ include file="../fragments/navbar.jspf"%>
</head>
<body>
	<!-- 	<div class="container-fluid"> -->
	<div class="container">
		<div style="text-align: center">
			<h4>
				<b>Add Multiple Contacts</b>
			</h4>
		</div>
		<form:form method="post" action="save" modelAttribute="contactForm">
			<table id="tableitems" class="table table_morecondensed table-responsive-sm  width=80%">
				<thead class="thead-secondary">
					<tr>
						<th>No.</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Phone</th>
					</tr>
				</thead>
				<c:forEach items="${contactForm.contacts}" var="contact"
					varStatus="status">
					<tr>
						<td align="center">${status.count}</td>
						<td><input class="form-control"
							name="contacts[${status.index}].firstname"
							value="${contact.firstname}" /></td>
						<td><input class="form-control"
							name="contacts[${status.index}].lastname"
							value="${contact.lastname}" /></td>
						<td><input class="form-control"
							name="contacts[${status.index}].email" value="${contact.email}" /></td>
						<td><input class="form-control"
							name="contacts[${status.index}].phone" value="${contact.phone}" /></td>
					</tr>
				</c:forEach>
			</table>
			<br />
			<input type="submit" class="btn btn-primary" value="Save" />
		</form:form>
	</div>
</body>
<%@ include file="../fragments/footer.jspf"%>
</html>
