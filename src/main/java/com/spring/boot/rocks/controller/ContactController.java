package com.spring.boot.rocks.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.boot.rocks.model.Contact;
import com.spring.boot.rocks.model.ContactForm;

@Controller
public class ContactController {

	private static List<Contact> contacts = new ArrayList<Contact>();

	static {
		contacts.add(new Contact("FirstName1", "LastName1", "Name1@domain.com", "123-456-7890"));
		contacts.add(new Contact("FirstName2", "LastName2", "Name2@domain.com", "113-456-7890"));
		contacts.add(new Contact("FirstName3", "LastName3", "Name3@domain.com", "111-456-7890"));
		contacts.add(new Contact("FirstName4", "LastName4", "Name4@domain.com", "111-156-7890"));
		contacts.add(new Contact("FirstName5", "LastName5", "Name5@domain.com", "111-116-7890"));
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView get() {

		ContactForm contactForm = new ContactForm();
		contactForm.setContacts(contacts);

		return new ModelAndView("add_contact", "contactForm", contactForm);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("contactForm") ContactForm contactForm) {
		List<Contact> contacts = contactForm.getContacts();

		if (null != contacts && contacts.size() > 0) {
			ContactController.contacts = contacts;
			System.out.println("Saving...");
			for (Contact contact : contacts) {
				System.out.printf(contact.getFirstname() + " " + contact.getLastname() + "\n");
			}
		}

		return new ModelAndView("show_contact", "contactForm", contactForm);
	}
}